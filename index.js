const request = require("request")
const fs = require("fs");
const login = require("facebook-chat-api");

var cke = "dotcom_session_key=s%3Avdm-9EvSeyiKNTc-Bnrw-pFOBa4K-gbg.1ZyvFqmfTaL3nz%2B9mEM3CbtKQY%2FKz%2FZM11JjELPRPK8; _ga=GA1.2.901555657.1541344763; _gid=GA1.2.911020384.1541344763; normalProb=0; lc=th; lname=%E0%B9%84%E0%B8%97%E0%B8%A2; bbl_cnt=9; currentChatCnt=1; _ga=GA1.2.901555657.1541344763; _gid=GA1.2.911020384.1541344763; _gat=1"
var status = true
var users = {}
const addPoint = () => request({
    method: 'GET',
    url: 'http://www.simsimi.com/finishMissA',
    qs: {
        mid: '3',
        pid: '10',
        upid: '6970303',
        checked: '["47981278"]'
    },
    headers: {
        'cache-control': 'no-cache',
        cookie: cke,
        'accept-language': 'th-TH,th;q=0.9,en;q=0.8',
        referer: 'http://www.simsimi.com/missA',
        'content-type': 'application/json; charset=utf-8',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
        accept: 'application/json, text/javascript, */*; q=0.01'
    }
});

const simsimi = (msg) => {

    return new Promise((resolve, reject) => {

        request({
            method: 'GET',
            url: 'http://www.simsimi.com/getRealtimeReq',
            qs: {
                lc: 'th',
                ft: '1',
                normalProb: '0',
                reqText: msg,
                status: 'W',
                talkCnt: '20'
            },
            headers: {
                cookie: cke,
                'postman-token': 'd0a220b0-2536-aeeb-2b38-499b8340e60a',
                'cache-control': 'no-cache',
                'accept-language': 'th-TH,th;q=0.9,en;q=0.8',
                referer: 'http://www.simsimi.com/AppCopyMain',
                'content-type': 'application/json; charset=utf-8',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
                'x-requested-with': 'XMLHttpRequest',
                accept: 'application/json, text/javascript, */*; q=0.01'
            }
        }, (error, response, body) => {
            console.log(body)
            resolve(JSON.parse(body))
        })
    })
}
login({
    appState: JSON.parse(fs.readFileSync('appstate.json', 'utf8'))
}, (err, api) => {
    if (err) return console.error(err);

    api.setOptions({
        listenEvents: true
    });

    api.listen((err, event) => {
        if (err) return console.error(err);

        api.markAsRead(event.threadID, (err) => {
            if (err) console.error(err);
        });

        switch (event.type) {
            case "message":
                if (!users.hasOwnProperty(event.threadID)) {
                    users[event.threadID] = true
                }
                if (event.body === '/start') {
                    status = true
                    api.sendMessage("start…", event.threadID);
                    return
                }

                if (event.body === '/stop') {
                    status = false
                    api.sendMessage("stop…", event.threadID);
                    return
                }
                if (!status) {
                    return
                }
                if (event.body === '/หยุดทำงาน') {
                    users[event.threadID] = false
                    return
                }
                if (event.body === '/เปิดทำงาน') {
                    users[event.threadID] = true
                    return
                }
                if (!users[event.threadID]) {
                    return
                }
                addPoint()
                simsimi(event.body).then(x => api.sendMessage("หุ่นยนต์ > " + x.respSentence, event.threadID))
                break;
            case "event":
                console.log(event);
                break;
        }
    });
});